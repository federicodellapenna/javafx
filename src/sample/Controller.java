package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;

public class Controller {
    private int clickCounter=1;
    @FXML
    private Button startTimerButton;
    @FXML
    private void clickOnButton(ActionEvent ae) {
        System.out.println("Clicked"+ clickCounter + " time" + (clickCounter++ > 1 ? "s":""));
        MioThread mt=new MioThread(startTimerButton);
        mt.start();
        System.out.println("Timer esaurito...");
        startTimerButton.setDisable(true);
        /*
        try{
            Thread.sleep(10000);
        }catch(InterruptedException e){
            e.printStackTrace();
        }
        System.out.println("Conteggio esaurito...");

         */
    }
}
