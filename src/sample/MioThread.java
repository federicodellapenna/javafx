package sample;

import javafx.scene.control.Button;
public class MioThread extends Thread{
    private Button startTimerButton;

    public MioThread(Button b){
        this.startTimerButton=b;
    }
    @Override
    public void run(){
        try{
            Thread.sleep(10000);
        }catch(InterruptedException e){
            e.printStackTrace();
        }
        System.out.println("Conteggio esaurito...");
        startTimerButton.setDisable(false);
    }
}
